# About

The BloonsTD6Bot is a farming bot for the popular tower defence game [_Bloons TD 6_](https://ninjakiwi.com/Games/Steam/Bloons-TD-6-Steam.html).
It is intended to be used to farm [_Insta-Monkeys_](https://bloons.fandom.com/wiki/Insta-Monkey) during time-limited
events such as Easter or Halloween, or to farm Monkey Knowledge/Money/XP during the off season.

## Cheating

Take time to read [BTD6 EULA](https://ninjakiwi.com/terms)


TL;DR ->

```...Without express written consent from Ninja Kiwi, any attempts to modify, hack, cheat, autoplay, bot, datasniff, data capture, interfere with, disrupt, reproduce, frame, copy, post, sell, scrape, transmit, clone accounts, clone characters, or create related or derivative works is expressly prohibited and immediately terminates the limited license granted and will lead to the automated or manual segregation and/or suspension of your account; any attempts to promote such proscribed usage through video, stream, social network, public display, digital communications, or any other mass audience channel is expressly prohibited and immediately terminates the limited license granted and will lead to the segregation and/or suspension of your account....```


This bot is NOT INTENDED for use with multiplayer aspects of the game including:
- Boss balloons
- Races
- Co-op

While you certainly may adapt this bot however you see fit, I will not be making any changes to this bot to make it cooperate with
multiplayer aspects of the game. Some design aspects (existing and future) will actually limit this unintentionally

Use this bot at your own risk :)

# Technologies

- [asyncio](https://docs.python.org/3/library/asyncio.html) - [asyncronous](https://realpython.com/python-async-features/) code execution
- [pytesseract](https://pypi.org/project/pytesseract/) - [image-to-text](https://tesseract-ocr.github.io/tessdoc/Home.html) engine
- [mouse](https://pypi.org/project/mouse/) - control system mouse
- [keyboard](https://pypi.org/project/keyboard/) - control system keyboard
- [pyautogui](https://pypi.org/project/PyAutoGUI/) (opencv-python, pillow) - image processing/detection/automation.

## Note

pyautogui provides just about all the functionalities provided by 'mouse' and 'keyboard'. In practice, I've found that sometimes
it's click/type methods don't work properly on my Windows machine. Using 'mouse' and 'keyboard' hasn't failed yet.


# History

This bot went through a few iterations:
1. btd6bot forked from [here](https://github.com/JSpeleers/BloonsTD6Bot), and updated slightly (see commit history)

This method was unfortunately unreliable and time consuming. To edit each required image for fuzzy-matching, took a lot of time,
and wasn't always accurate during execution. On top of that, pyautogui is known to be slow (seconds instead of milliseconds) when
performing it's image searching.

2. [macrobot](https://gitlab.com/triplejay2013-personal-projects/projects/macrobot)

I wanted to create something that didnt' rely on image searching at all. So this was an attempt to create a tool that you
could 'teach' to play the game, then it would mimic you. This unfortunately was also unreliable because the time to execute
the macro wasn't accounted for properly. This would cause the macro to fall out of sync by a few milliseconds per instruction.
By the end of a long-running game, it would be off by a few seconds. This wasn't ideal.

3. [btd6bot](https://gitlab.com/triplejay2013-personal-projects/projects/btd6bot)

This combines the previous to methods in some ways. It takes a screenshot of the statusline (where the health, gold and rounds are displayed),
then `pytesseract` parses them for text, and returns that to the caller. This happens asyncronously (on it's own event loop). Events can also
be registered which will automatically find and interact with pop-up elements (game over, overwrite save, collection, etc.). Coordinates are
pre-determined and an event list is configured prior to execution. These coordinates are used with preset hotkeys to perform tower placements,
upgrades, etc.

# Acknowledgements

Initial inspiration for this project came from:

- [here](https://github.com/JSpeleers/BloonsTD6Bot)


# Install

TBD - .exe coming at some point in the future. For now just follow Devleoper install

## Developer Install

- Install python3 (expects at least version 3.8, lower versions may work but are unsupported)
- (OPTIONAL) Create a Python virtual environment: `$ python3 -m venv btd6bot`
- (OPTIONAL) Activate the virtual environment
    - Windows: `$ btd6bot\Scripts\activate`
    - Unix & MacOS: `$ source btd6bot/bin/activate`
- Install requirements: `$ pip3 install -r requirements.txt`
- Take not of .vscode for launch config options
- Install Prerequisites
- On windows you may need to set PYTHONPATH: `$ENV:PYTHONPATH = "src"`
## Prerequisites

- _Bloons TD 6_ on [Steam](https://store.steampowered.com/app/960090/Bloons_TD_6/)
- Python3
- [tesseract](https://tesseract-ocr.github.io/tessdoc/Home.html#binaries)

## Start

Starting the bot can be done using the following steps:

1. Run _Bloons TD 6_  from Steam, full-screen (other modes are untested)
2. Run the bot: `$ python3 cli.py <map_name> [OPTIONS]`
3. If installed with pip: `btd6bot <map_name> [OPTIONS]`
4. TBD - If installed with executable, just run the executable

## Stop

Stopping this bot, while it is clicking or searching the screen, is done in a few ways:

- Move mouse to the top-left corner of the screen. (coordinates 0, 0). This will initiate a cancel run (cancels current run)
- hold `Ctrl+C` (cancels current run)
- kill the script directly (ctrl-c) (cancels all runs)
- Move mouse  to top-right corner of the screen. This initiates shutdown (cancels all runs)

# Gotchas

Just a few tips for developing new maps, or to be aware of when running exist maps

## Registered Events (pyautogui locateCenterOnScreen)

registered events don't trigger. The registered events loop on a separate async event loop, and take a few seconds to process each. Depending
on the amount of events that are registered, it just takes time to react appropriately. Also I took my own screenshots of particular events
to search for, these may not always work, or be accurate for future updates to BTD6. If events are a hassle, disable them with `DISABLE_EVENT_REGISTER`,
and manage these pop-up events on your own.

## Screengrabs (StatusBar/statusline)

For time-sensitive placements, it may be prudent to `Start(False)` to slow down the game.  The Screengrab of the status line and processing using
tesseract, takes about 2s (1.8s on my machine). In particluar, the gold value tends to be more inconsistent. You can increase the `SCREENGRAB_RETRY_COUNT`,
or manually pass in a value to the Map.status call. The drawback is that this will take more time per condition checked. The payoff is that your conditions
are more likely to be accurate.

# Roadmap

- Perform Hero Check before each run
- implement event checker that validates we are on main screen. The user is responsible for this for now
- implement pause signal that will pause the run, and pull up the pause menu
- outlier check from screengrab. See known issues for pytesseract

## Known Issues

- Sometimes pytesseract reads the wrong number (usually larger by a factor of 10), which causes
    conditions to return True at the wrong time. This is rare (once out of thousands of checks), but still a hassle when it occurs
- collecting collection chests doesn't work since they provide 3 instans, instead of two. There is no detection for this yet, and will
    cause unexpected behavior :/

# Development

## Packaging / Build / Installation

Generic steps to build and/or install locally (Windows)

```bash
# Update your PATH to include script location
cd btd6bot
pip3 install -r build-requirements.txt
python3 -m build --sdist --wheel
pip3 install .
btd6bot.exe
```

The repo has things already packaged, so unless you make changes, you just need to pip3 install the local package, and update your PATH.
The `btd6bot.exe` will then be available anywhere in Windows CMD prompt