"""Help locate coordinates on screen."""
import keyboard
import mouse
import pyautogui as pag

RES = pag.size()
ONCE = True

print("Press Ctrl-C to quit")
while True:
    if keyboard.is_pressed("ctrl+c"):
        break
    # Wait until we are done recording
    x, y = mouse.get_position()
    # Raw x, y
    position = f"X: {str(x).rjust(6)} Y: {str(y).rjust(6)} "
    # Pct x, y
    position += f"X-pct: {str(x / RES[0]).rjust(20)} Y-pct: {str(y / RES[1]).rjust(20)} "
    # Calculated x, y
    position += f"Calc-X: {str(int(RES[0] * (x / RES[0]))).rjust(6)} Calc-Y: {str(int(RES[1] * (y / RES[1]))).rjust(6)}"
    if mouse.is_pressed(mouse.LEFT):
        # Save only coordinates on primary monitor
        if ONCE and x > 0 and y > 0 and x < RES[0] and y < RES[1]:
            print(position)
            ONCE = False
    else:
        ONCE = True
        print(position, end="")
        print("\b" * len(position), end="\r", flush=True)
print("\n")
