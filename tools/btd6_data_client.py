"""
Sources:
- https://github.com/hemisemidemipresent/cyberquincy/blob/master/jsons/income-abr.json
- https://github.com/hemisemidemipresent/cyberquincy/blob/master/jsons/income-normal.json
- https://github.com/hemisemidemipresent/cyberquincy/blob/master/jsons/costs.json
- https://github.com/hemisemidemipresent/cyberquincy/blob/master/helpers/bloons-general.js
SEE 'https://github.com/hemisemidemipresent/cyberquincy/blob/masterparser/' for more info on how data is formatted
"""

import json
import re
from pathlib import Path
from typing import Dict

import requests

EXT = Path(__file__).parent / "external"
OUT = Path(__file__).parent.parent / "src/btd6bot/app/vars.py"
HEADER = """\"\"\"The contents of this file are AUTO GENERATED.
DO NOT MODIFY THIS FILE. Instead run `tools/data_api.py` to generate and modify new data.

The contents of this file are formatted as such:

EXTERNAL_VARS = {
    "GENERAL": {
        ...
    },
    "COST": {
        ...
    },
    "ROUND": {
        ...
    },
    "INCOME": {
        "abr": [...],
        "normal": [...],
    }
}
\"\"\"\n"""

def load_general_vars() -> Dict:
    """Load general constant information from quincybot.
    
    A lot of their information is compiled externally, but they provide some global constants only
    in their javascript implementation (not true javascript, built into framework). This will parse
    that file for constant values, and submit them to a python dicitonary.
    """
    # Load const vars (even multiline values)
    REGEX = re.compile(r'^const.*(?:;|{[^}]*})', re.MULTILINE)
    JS_FILE = "bloons-general.js"
    js_vars = dict()

    res = requests.get("https://raw.githubusercontent.com/hemisemidemipresent/cyberquincy/master/helpers/bloons-general.js")
    content = res.content.decode('utf-8')
    with open(EXT / JS_FILE, "w") as f:
        f.write(content)

    for match in REGEX.findall(content):
        # Split into the name of the variable, and the assigned values
        parsed = match.split("=")
        # Strip out unnecessary vals
        parsed[0] = re.sub("const", "", parsed[0])
        parsed[1] = re.sub(";", "", parsed[1])
        # Remove whitespace
        parsed[0] = parsed[0].strip()
        parsed[1] = parsed[1].strip()
        # Evaluate RHS (may not work with all types, but for lists and json types; should be fine)
        parsed[1] = eval(parsed[1])
        # Match key-value with the name of the var: value of the var
        js_vars[parsed[0]] = parsed[1]
    return js_vars

def load_cost_vars():
    """Load costs for each tower."""
    JSON_FILE = "cost.json"

    res = requests.get("https://raw.githubusercontent.com/hemisemidemipresent/cyberquincy/master/jsons/costs.json")
    content = res.json()
    with open(EXT / JSON_FILE, "w") as f:
        f.write(json.dumps(content, indent=2))

    return content
    

def load_round_vars():
    """Load income gained per round, and other info."""
    JSON_FILE = "rounds.json"

    res = requests.get("https://raw.githubusercontent.com/hemisemidemipresent/cyberquincy/master/jsons/round2.json")
    content = res.json()
    with open(EXT / JSON_FILE, "w") as f:
        f.write(json.dumps(content, indent=2))
    
    return content

def load_income_vars():
    """Load income info about each round."""
    res1 = requests.get("https://raw.githubusercontent.com/hemisemidemipresent/cyberquincy/master/jsons/income-abr.json")
    content1 = res1.json()
    with open(EXT / "income-abr.json", "w") as f:
        f.write(json.dumps(content1, indent=2))

    res2 = requests.get("https://raw.githubusercontent.com/hemisemidemipresent/cyberquincy/master/jsons/income-normal.json")
    content2 = res2.json()
    with open(EXT / "income-normal.json", "w") as f:
        f.write(json.dumps(content2, indent=2))

    return {"abr": content1, "normal": content2}

OUTPUT = {}
OUTPUT["GENERAL"] = load_general_vars()
OUTPUT["COST"] = load_cost_vars()
OUTPUT["ROUND"] = load_round_vars()
OUTPUT["INCOME"] = load_income_vars()

if input(f"Write to {OUT} (y/n): ")[0].lower() in ["y"]:
    with open(OUT, "w") as f:
        f.write(HEADER)
        f.write("EXTERNAL_VARS = ")
        f.write(json.dumps(OUTPUT, indent=2))
