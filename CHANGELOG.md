# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.3] - 2022-05-05
### Added/Refactored
Refactored code. Load data from external sources in preparation for future implementations

## [0.1.2] - 2022-04-18
### Added
Misc bug fixes and code optimizations. Semi-Steady round execution

## [0.1.1] - 2022-04-16
### Added
Fixed timing issues with mouse Lock clicks

## [0.1.0] - 2022-04-15
### Added
Added Locks to help control when events (background and main events) can control the keyboard
This helps prevent misclicks and clashing.

- Mutex/Logging.

## [Unreleased]
### Added
- Initial Release (intermittently functional)

[0.1.3]: https://gitlab.com/triplejay2013/projects/btd6bot/-/tags/v0.1.3
[0.1.2]: https://gitlab.com/triplejay2013/projects/btd6bot/-/tags/v0.1.2
[0.1.1]: https://gitlab.com/triplejay2013/projects/btd6bot/-/tags/v0.1.1
[0.1.0]: https://gitlab.com/triplejay2013/projects/btd6bot/-/tags/v0.1.0
[0.0.0]: https://gitlab.com/triplejay2013/dotfiles/compare/v0.0.0...HEAD
